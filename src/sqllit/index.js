import path from 'path'
import { Sequelize, DataTypes } from 'sequelize'

let dbFile = path.resolve(__dirname, '../../../', 'lotus-novel', 'db/lotus.db')

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: dbFile
});

// class Novel extends Model { }

const Novel = sequelize.define('Novel', {
  id: {
    type: DataTypes.TEXT,
    primaryKey: true,
  },
  name: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  },
  createdAt: 'createTimestamp',
  updatedAt: 'updateTimestamp',
}, {
  modelName: 'novels' // We need to choose the model name
})

Novel.create({
  id: '123',
  name: '123_name',
  description: '123_description'
})
