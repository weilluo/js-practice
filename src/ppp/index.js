import '@babel/polyfill';

import fs from 'fs';
import path from 'path';

import fetch from 'node-fetch';
import { URLSearchParams } from 'url';

const totalPage = 63;
const pageSize = 200;

const writeStream = fs.createWriteStream(path.resolve(__dirname, 'output', `file.xls`), { encoding: 'utf-8' });

// 由于 NodeJS只支持 ascii、utf8、base64、binary 编码方式, 不支持 MS 的 utf-8 + BOM 格式, 所以 excel 打开中文乱码。
// 解决方法：既然excel需要BOM，写入数据前先加入一个BOM。utf-8对应的BOM是EF BB BF
let bom = new Buffer('\xEF\xBB\xBF', 'binary');
writeStream.write(bom);

const header = "省" + "\t" + "市" + "\t" + "区(县)" + "\t" + " 所属行业" + "\t" + "工程分类" + "\t" + "所属阶段" + "\t" + "项目名称" + "\t" + "项目总投资" + "\t" + "发起时间" + "\t" + "回报机制" + "\t" + "合作期限(识别)" + "\t" + "项目运作方式(识别)" + "\n";
writeStream.write(header);

async function sendRequest(queryPage = 1) {
    const params = new URLSearchParams();
    params.append('queryPage', queryPage);
    params.append('pageSize', pageSize);

    try {
        let res = await fetch('http://www.cpppc.org:8086/pppcentral/map/getPPPList.do', { method: 'POST', body: params });
        let json = await res.json();

        const str = json.list
            .map(item => `"${item.PRV1}"\t"${item.PRV2 || ''}"\t"${item.PRV3 || ''}"\t"${item.IVALUE}"\t"${item.IVALUE2}"\t"${item.PROJ_STATE_NAME}"\t"${item.PROJ_NAME}"\t"${item.INVEST_COUNT}"\t"${item.START_TIME}"\t"${item.RETURN_MODE_NAME}"\t"${item.ESTIMATE_COPER}"\t"${item.OPERATE_MODE_NAME}"`)
            .join("\n");
        await write(str + "\n");

    } catch (err) {
        console.error(err);
        console.error('Error: pageSize =', 1);
    }
}

function write(str) {
    return new Promise((resolve, reject) => {
        writeStream.write(str, (err) => {
            if (err) {
                console.error(err);
                reject();
            } else {
                resolve();
            }
        })
    });
}

for (let i = 1; i <= totalPage; i++) {
    setTimeout(() => sendRequest(i), 2500);
}
