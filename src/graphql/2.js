var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { graphql, buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    rollDice(numDice: Int!, numSides: Int): [Int]
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  rollDice({ numDice, numSides }) {
    return [numDice + numSides]
  }
}

// Run the GraphQL query '{ hello }' and print out the response
let query = `
  query {
    rollDice(numDice: 3, numSides: 100)
  }
`

graphql(schema, query, root).then((response) => {
  console.log(JSON.stringify(response));
});

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000, () => {
  console.log('Running a GraphQL API server at localhost:4000/graphql');
});
