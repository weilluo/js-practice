var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { graphql, buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type RandomDie {
    numSides: Int!
    rollOnce: Int!
    roll(numRolls: Int!): [Int]
  }
 
  type Query {
    getDie(numSides: Int): RandomDie
  }
`);

// This class implements the RandomDie GraphQL type
class RandomDie {
  constructor(numSides) {
    this.numSides = numSides;
  }

  rollOnce() {
    return 1 + Math.floor(Math.random() * this.numSides);
  }

  roll({ numRolls }) {
    var output = [];
    for (var i = 0; i < numRolls; i++) {
      output.push(this.rollOnce());
    }
    return output;
  }
}

// The root provides the top-level API endpoints
var root = {
  getDie: ({ numSides }) => {
    return new RandomDie(numSides || 6);
  }
}

let query = `
  query {
    getDie(numSides: 6) {
      rollOnce
      roll(numRolls: 3)
    }
  }
`

graphql(schema, query, root).then((response) => {
  console.log(JSON.stringify(response));
});

// query = `
// query GetDie($numSides: Int!) {
//   getDie(numSides: $numSides) {
//     rollOnce,
//     roll(numRolls: 5)
//   }
// }
// `
// variables = { "numSides": 10 }

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000, () => {
  console.log('Running a GraphQL API server at localhost:4000/graphql');
});
