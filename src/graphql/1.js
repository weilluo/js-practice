var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { graphql, buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    name: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  name: 'English book'
};

// Run the GraphQL query '{ hello }' and print out the response
let query = `
  { name }
`

graphql(schema, query, root).then((response) => {
  console.log(response);
});

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000, () => {
  console.log('Running a GraphQL API server at localhost:4000/graphql');
});
