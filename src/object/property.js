var aaa = {
    a: 1
}

Object.defineProperty(aaa, 'a', {
    configurable: false,
    writable: false,
    // value: 2
})

// aaa.a = 123
delete aaa.a;

console.log(aaa.a);
