const nodemailer = require('nodemailer');

let emailConfig = {
    host: 'email-smtp.us-east-1.amazonaws.com',
    port: '465',
    secure: true,
    auth: {
        user: 'xxxxx',
        pass: 'xxxxx',
    },
    tls: {
        rejectUnauthorized: true,
    },
    proxy: '',
};

let transporter = nodemailer.createTransport(emailConfig);

transporter.sendMail({
    from: 'luowei---010101@163.com',
    to: '544765812@qq.com',
    subject: 'test subject',
    text: 'test text',
}, (error, info) => {
    if (error) {
        console.error(error)
    } else {
        console.log(info)
    }
})
