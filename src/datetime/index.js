class DurationFormatter {
    constructor(duration, pattern) {
        this.duration = duration
        this.pattern = pattern
    }

    convertToString() {
        let result = ''
        let resultJson = {}

        const patterns = this.pattern.split('')

        patterns.forEach(pattern => {
            if (pattern === 'd') {
                //
                resultJson.days = Number.parseInt('' + this.duration / (3600 * 24))
                if (resultJson.days > 0) {
                    result += `${resultJson.days}天`
                }
            } else if (pattern === 'h') {
                //
                let duration = this.duration - (resultJson.days || 0) * 3600 * 24
                resultJson.hours = Number.parseInt('' + duration / 3600)
                if (resultJson.hours > 0) {
                    result += `${resultJson.hours}小时`
                }
            } else if (pattern === 'm') {
                //
                let duration =
                    this.duration - (resultJson.days || 0) * 3600 * 24 - (resultJson.hours || 0) * 3600
                resultJson.minutes = Number.parseInt('' + duration / 60)
                if (resultJson.minutes > 0) {
                    result += `${resultJson.minutes}分钟`
                }
            }
        })

        return result
    }
}

var formatter = new DurationFormatter(60, 'dhm')

console.log(formatter.convertToString())
