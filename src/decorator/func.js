// Why legacy?
// https://github.com/loganfsmyth/babel-plugin-transform-decorators-legacy#why-legacy

function log(target, name, descriptor) {
    var oldValue = descriptor.value;
    descriptor.value = function() {
        console.log('Math.add, params is ', arguments);
        return oldValue.apply(undefined, arguments);
    }
    return descriptor;
}

class Math {
    @log
    add(a, b) {
        return a + b;
    }
}

let math = new Math();
let result = math.add(1, 2);

console.log(result);
